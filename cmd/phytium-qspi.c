// SPDX-License-Identifier: GPL-2.0+
/*
 * (C) Copyright 2000
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 */

/*
 * FLASH support
 */
#include <common.h>
#include <command.h>

#if defined(CONFIG_CMD_MTDPARTS)
#include <jffs2/jffs2.h>

/* partition handling routines */
int mtdparts_init(void);
int mtd_id_parse(const char *id, const char **ret_id, u8 *dev_type, u8 *dev_num);
int find_dev_and_part(const char *id, struct mtd_device **dev,
		u8 *part_num, struct part_info **part);
		
//extern int spi_erase_sector(uint32_t BlockAddress);
extern int qspinor_erase(uint64_t addr, int len)
extern int spi_page_program(uint32_t *src_addr, uint32_t *page_addr, uint32_t pp_num)；

//extern int qspinor_programe_data(uint32_t *src_addr, uint64_t page_addr, uint32_t len);
//extern void qspinor_write(uint32_t *src_addr,  uint64_t page_addr,uint32_t len)；

#endif


//extern void spi_page_program(u32 src_addr, u32 page_addr, u32 pp_num);
//#ifdef CONFIG_MTD_NOR_FLASH
//#include <flash.h>
//#include <mtd/cfi_flash.h>
//extern flash_info_t flash_info[];	/* info for FLASH chips */


/*
 * The user interface starts numbering for Flash banks with 1
 * for historical reasons.
 */

/*
 * this routine looks for an abbreviated flash range specification.
 * the syntax is B:SF[-SL], where B is the bank number, SF is the first
 * sector to erase, and SL is the last sector to erase (defaults to SF).
 * bank numbers start at 1 to be consistent with other specs, sector numbers
 * start at zero.
 *
 * returns:	1	- correct spec; *pinfo, *psf and *psl are
 *			  set appropriately
 *		0	- doesn't look like an abbreviated spec
 *		-1	- looks like an abbreviated spec, but got
 *			  a parsing error, a number out of range,
 *			  or an invalid flash bank.
 */
static int do_flerase(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	
	
	static unsigned long start_addr = 0, length = 0;
	u64 sector_num, i;

	if(argc != 3)
		return CMD_RET_USAGE;
	
	start_addr = simple_strtoul(argv[1], NULL, 16);
	length = simple_strtoul(argv[2], NULL, 16);

	//sector_num = length / (64 * 1024);

	printf("erase qspi flash\n");
	
	         qspinor_erase(start_addr,length);//JHB
		/*---------	 OLD 
			 for(i = 0; i < sector_num; i++)
		         spi_erase_sector(start_addr + i * (64 * 1024));
	         
			 
			------------------- */
	printf("erase done\n");
	

	return 0;
}

static int do_flash_write(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	static unsigned long src_addr = 0, length = 0;
	static unsigned long dst_addr = 0;
	
	if(argc != 4)
		return CMD_RET_USAGE;

	src_addr = simple_strtoul(argv[1], NULL, 16);
	dst_addr = simple_strtoul(argv[2], NULL, 16);
	length = simple_strtoul(argv[3], NULL, 16);

	printf("write ...\n");
	spi_page_program(src_addr, dst_addr, length);// old 
	//qspinor_write(src_addr, dst_addr, length);// JHB 
	printf("write done\n");

	return 0;
}


U_BOOT_CMD(
	flashe,  3,   0,  do_flerase, 
	"erase QSPI FLASH \n",
	"start end\n"
	"    - erase FLASH from addr 'start' to addr 'end'\n"
	"    - note: erase must more than one sector [64K*N]\n"
	
	
);
U_BOOT_CMD(
	flashw,  4,  0,   do_flash_write,
	"qspi flash write",
	"write [source address] [destion address] [length]"
);


