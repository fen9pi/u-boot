#include <command.h>
#include <bootm.h>
#include <image.h>
#include <auth_mod.h>
#include <d2000_auth_img.h>
#include <bootstage.h>

int image_check_auth(unsigned int image_id, unsigned long img_addr,uintptr_t len)
{
	
	image_header_t *hdr = (image_header_t *)img_addr;
	uintptr_t data = (uintptr_t)hdr;
	//uintptr_t len = sizeof(image_header_t) + image_get_data_size(hdr);
	
	printf("image_check_auth:image_id=%d,  len=%ld\n", image_id, len);
	
	int err = auth_image(image_id, data, len);
	printf("image_check_auth:image_id=%d, len=%ld, err=%d\n", image_id, len,err);
	return err;
}


static int do_boot_check(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	unsigned long kernel_addr;
	unsigned long kernel_len;
	unsigned long pubkey_bin_addr;
	unsigned long bin_len;	
	unsigned long pubkey_crt_addr;
	unsigned long pubkey_crt_len;
	unsigned long img_crt_addr;
	unsigned long img_crt_len;


	kernel_addr = simple_strtoul(argv[1], NULL, 16)&0xffffffff;
	kernel_len = simple_strtoul(argv[2],NULL,16);
	pubkey_bin_addr = simple_strtoul(argv[3], NULL, 16);
	bin_len = simple_strtoul(argv[4],NULL,16);
	pubkey_crt_addr = simple_strtoul(argv[5], NULL, 16);
	pubkey_crt_len = simple_strtoul(argv[6],NULL,16);
	img_crt_addr = simple_strtoul(argv[7], NULL, 16);
	img_crt_len = simple_strtoul(argv[8],NULL,16);
	

	auth_mod_init(pubkey_bin_addr,bin_len,pubkey_crt_addr,pubkey_crt_len,img_crt_addr,img_crt_len);


	if (image_check_auth(NT_KERNEL_IMG_ID, kernel_addr,kernel_len) != 0) {
		printf("=========KERNEL IMG VERIFY FAILD!!!\n");
		bootstage_error(BOOTSTAGE_ID_CHECK_AUTH);
		return NULL;
	}
	return 0;
}

U_BOOT_CMD(checkauth, 9, 1, do_boot_check,
	"boot check auth",
	" arg0 ...\n");

