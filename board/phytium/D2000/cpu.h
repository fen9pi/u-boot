/*
 *  * (C) Copyright 2013
 *   * Phytium Technology Co.,Ltd <www.phytium.com>
 *    *
 *     * SPDX-License-Identifier:	GPL-2.0+
 *      */

#ifndef _D2000_CPU_H
#define _D2000_CPU_H

typedef struct {
	uint32_t Flash_Index;
	uint32_t Flash_Write;
	uint32_t Flash_Erase;
	uint32_t Flash_Pp;
}FLASH_CMD_INFO;
#define PHYTIUM_OEM_SVC_GET_FLASH_CMD  0xC200000C


/* FLUSH L3 CASHE */
#define HNF_COUNT           0x8
#define HNF_PSTATE_REQ      (HNF_BASE + 0x10)
#define HNF_PSTATE_STAT     (HNF_BASE + 0x18)
#define HNF_PSTATE_OFF      0x0
#define HNF_PSTATE_SFONLY   0x1
#define HNF_PSTATE_HALF     0x2
#define HNF_PSTATE_FULL     0x3
#define HNF_STRIDE          0x10000
#define HNF_BASE		(unsigned long)(0x3A200000)

#endif /* _D2000_CPU_H */
