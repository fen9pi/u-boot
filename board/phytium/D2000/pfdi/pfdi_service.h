typedef unsigned long u_register_t;

#define PFDI_REGISTER		0xC2000012
#define PFDI_DONE			0xC2000013

typedef struct pfdi_vectors_s {
	u_register_t system_off_entry;
	u_register_t system_reset_entry;
	u_register_t suspend_start_entry;
	u_register_t suspend_end_entry;
	u_register_t suspend_finish_entry;
} pfdi_vectors_t;

#define PWR_CTR0_REG	0x28180480
#define PWR_CTR1_REG	0x28180484

#define S3_CLEAN_CPLD			1
#define REBOOT_CPLD				4
#define S3_SETUP_CPLD			8
#define SHUTDOWN_CPLD			12

