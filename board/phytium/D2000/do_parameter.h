/*
 * Copyright (c) 2015 Google, Inc
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __FIX_ENV_H
#define __FIX_ENV_H


#define OEM_COMMON_INFO		0xC300FF10
#define COMMON_CPU_INFO			0X0


#define OEM_PEU_INFO	0xC300FF11
#define PEU_BASE_INFO		0X0


#define OEM_OS_INFO		0xC300FF12
#define OS_INITRD_SIZE		0X0
#define OS_INITRD_START     0X1

#define	MEM_REGIONS			0xC2000005			//phytium_sip 有,就不用在OEM中重新注册一个了

typedef struct core_info {
	uint64_t cpu_map;
	uint32_t tick_frq;
}__attribute__((aligned(sizeof(unsigned long)))) core_info_t;

typedef struct peu_info {
	uint32_t peu_init_stat;
}__attribute__((aligned(sizeof(unsigned long)))) peu_info_t;

void board_fix_env(void);
uint32_t get_cpu_info(core_info_t *ptr);
uint32_t get_peu_info(peu_info_t *ptr);

typedef struct mem_block_pbf {
    uint64_t mb_start;
    uint64_t mb_size;
    uint64_t mb_node_id;
} mem_block_t;
typedef struct mem_region_pbf {
    uint64_t mb_count;
    mem_block_t mb_blocks[2];
} mem_region_t;
uint32_t get_dram_info(mem_region_t *ptr);

#endif
