#ifndef _PARAMETER_H
#define _PARAMETER_H

#define USE_PARAMETER		//采用读取参数表

#define O_PARAMETER_BASE			0x17e800	/*1530k*/

#define PM_PLL_OFFSET				(0x0)
#define PM_PCIE_OFFSET				(0x100)
#define PM_COMMON_OFFSET			(0x200)
#define PM_MCU_OFFSET				(0x300)

/*-------------------PLL------------------*/
typedef struct pll_config {
	uint32_t magic;
	uint32_t version;
	uint32_t size;
	uint8_t rev1[4];
	uint32_t core_pll;	
	uint32_t res1;	
	uint32_t lmu_pll;	
	uint32_t res2;
	uint32_t res3;
	uint32_t res4;
	uint32_t res5;
}__attribute__((aligned(sizeof(unsigned long)))) pll_config_t;

/*-------------------PEU------------------*/
typedef struct pcu_ctr{
	uint32_t base_config[3];
	uint32_t equalization[3];
	uint8_t rev[80];
}__attribute__((aligned(sizeof(unsigned long)))) peu_ctr_t;

typedef struct pcu_config {
	uint32_t magic;
	uint32_t version;
	uint32_t size;
	uint8_t rev1[4];
	uint32_t independent_tree;
	uint32_t base_cfg;
	uint8_t rev2[16];
	peu_ctr_t ctr_cfg[2];
}__attribute__((aligned(sizeof(unsigned long)))) peu_config_t;

/*-------------------COMMON------------------*/
typedef struct common_config {
	uint32_t magic;
	uint32_t version;
	uint32_t size;
	uint8_t rev1[4];
	uint64_t core_bit_map;
}__attribute__((aligned(sizeof(unsigned long)))) common_config_t;

/*-------------------MCU------------------*/
typedef struct ddr_spd {
    /******************* read from spd *****************/
    uint8_t  dimm_type;     /* 1: RDIMM;2: UDIMM;3: SODIMM;4: LRDIMM */
    uint8_t  data_width;      /* 0: x4; 1: x8; 2: x16 */
    uint8_t  mirror_type;     /* 0: stardard; 1: mirror */
    uint8_t  ecc_type;       /* 0: no-ecc; 1:ecc */
    uint8_t  dram_type;       /* 0xB: DDR3; 0xC: DDR4 */
    uint8_t  rank_num;
    uint8_t  row_num;
    uint8_t  col_num;

    uint8_t  bg_num;		/*only DDR4*/
    uint8_t  bank_num;
    uint16_t module_manufacturer_id;
    uint16_t tAAmin;
    uint16_t tRCDmin;

    uint16_t tRPmin;
    uint16_t tRASmin;
    uint16_t tRCmin;
    uint16_t tFAWmin;

    uint16_t tRRD_Smin;		/*only DDR4*/
    uint16_t tRRD_Lmin;		/*only DDR4*/
    uint16_t tCCD_Lmin;		/*only DDR4*/
    uint16_t tWRmin;

    uint16_t tWTR_Smin;		/*only DDR4*/
    uint16_t tWTR_Lmin;		/*only DDR4*/
    uint16_t tWTRmin;		/*only DDR3*/
    uint16_t tRRDmin;		/*only DDR3*/

    /******************* RCD control words *****************/
    uint8_t  F0RC03; /*bit[3:2]:CS         bit[1:0]:CA  */
    uint8_t  F0RC04; /*bit[3:2]:ODT        bit[1:0]:CKE */
    uint8_t  F0RC05; /*bit[3:2]:CLK-A side bit[1:0]:CLK-B side */
    uint8_t  BC00;
    uint8_t  BC01;
    uint8_t  BC02;
    uint8_t  BC03;
    uint8_t  BC04;

    uint8_t  BC05;
    uint8_t  F5BC5x;
    uint8_t  F5BC6x;
/******************* LRDIMM special *****************/
    uint8_t  vrefDQ_PR0;  /*Byte 140 (0x08C) (Load Reduced): DRAM VrefDQ for Package Rank 0*/
    uint8_t  vrefDQ_MDRAM;/* Byte 144 (0x090) (Load Reduced): Data Buffer VrefDQ for DRAM Interface*/
    uint8_t  RTT_MDRAM_1866; /*Byte 145 (0x091) (Load Reduced): Data Buffer MDQ Drive Strength and RTT for data rate < 1866 */
    uint8_t  RTT_MDRAM_2400; /*Byte 146 (0x092) (Load Reduced): Data Buffer MDQ Drive Strength and RTT for 1866<datarate<2400 */
    uint8_t  RTT_MDRAM_3200; /*Byte 147 (0x093) (Load Reduced): Data Buffer MDQ Drive Strength and RTT for 2400<datarate<3200 */

    uint8_t  Drive_DRAM;  /*Byte 148 (0x094) (Load Reduced): DRAM Drive Strength */
    uint8_t  ODT_DRAM_1866; /* Byte 149 (0x095) (Load Reduced): DRAM ODT (RTT_WR and RTT_NOM) for data rate < 1866 */
    uint8_t  ODT_DRAM_2400; /* Byte 150 (0x096) (Load Reduced): DRAM ODT (RTT_WR and RTT_NOM) for 1866 < data rate < 2400 */
    uint8_t  ODT_DRAM_3200; /* Byte 151 (0x097) (Load Reduced): DRAM ODT (RTT_WR and RTT_NOM) for 2400 < data rate < 3200 */
    uint8_t  PARK_DRAM_1866; /* Byte 152 (0x098) (Load Reduced): DRAM ODT (RTT_PARK) for data rate < 1866 */
    uint8_t  PARK_DRAM_2400; /* Byte 153 (0x099) (Load Reduced): DRAM ODT (RTT_PARK) for 1866 < data rate < 2400 */
    uint8_t  PARK_DRAM_3200; /* Byte 154 (0x09A) (Load Reduced): DRAM ODT (RTT_PARK) for 2400 < data rate  < 3200 */
    uint8_t  rcd_num;   /* Registers used on RDIMM */

}__attribute__((aligned(sizeof(unsigned long)))) ddr_spd_info_t;

typedef struct mcu_config { 
    uint32_t magic;
	uint32_t version;
	uint32_t size;
	uint8_t rev1[4];

    uint8_t ch_enable;
    uint8_t ecc_enable;
    uint8_t dm_enable;
	uint8_t force_spd_enable;
    uint8_t misc_enable;	/*0:read spd req 1:use margin 2: s3 back devinit 3: cmd 2T mode 4:dual dimm*/
    uint8_t train_debug;
    uint8_t train_recover;
	
    uint8_t rev2[9];

    ddr_spd_info_t ddr_spd_info[2];
}__attribute__((aligned(sizeof(unsigned long)))) mcu_config_t;

void get_pm_pll_info(pll_config_t *pm_data);
void get_pm_peu_info(peu_config_t *pm_data);
void get_pm_common_info(common_config_t *pm_data);
void get_pm_mcu_info(mcu_config_t *pm_data);
#endif
