#ifndef __FT_2004_CONFIG_H__
#define __FT_2004_CONFIG_H__

#define PHYS_SDRAM_1			0x80000000  /* SDRAM Bank #1 start address */
#define PHYS_SDRAM_1_SIZE		0x7B000000  
#define CONFIG_SYS_SDRAM_BASE   PHYS_SDRAM_1
#define PHYS_SDRAM_2                    0x2000000000
#define PHYS_SDRAM_2_SIZE               0x0
#define CONFIG_SYS_DRAM_TEST

#define CONFIG_SYS_LOAD_ADDR		(CONFIG_SYS_SDRAM_BASE + 0x10000000)
		
/* SIZE of malloc pool */
#define CONFIG_ENV_SIZE 4096
#define CONFIG_SYS_MALLOC_LEN   (1 * 1024 * 1024  + CONFIG_ENV_SIZE)
#define CONFIG_BOARD_EARLY_INIT_F
#define CONFIG_BOARD_EARLY_INIT_R

#define CONFIG_SYS_INIT_SP_ADDR		(0x29800000 + 0x1a000)

/* PCI CONFIG */
#define CONFIG_SYS_PCI_64BIT    1
#define CONFIG_PCI_SCAN_SHOW

/* SCSI */
#define CONFIG_SYS_SCSI_MAX_SCSI_ID 4
#define CONFIG_SYS_SCSI_MAX_LUN 1
#define CONFIG_SYS_SCSI_MAX_DEVICE 128
#define CONFIG_SCSI_AHCI_PLAT
#define CONFIG_SYS_SATA_MAX_DEVICE 4

/* USB */
#define CONFIG_USB_OHCI_NEW
#define CONFIG_SYS_USB_OHCI_MAX_ROOT_PORTS 1

/*boot*/
#define CONFIG_SYS_BOOTM_LEN    (60 * 1024 * 1024)



#define CONFIG_EXTRA_ENV_SETTINGS	\
			"ethaddr=00:11:22:33:44:55\0"	\
			"eth1addr=10:22:33:44:55:66\0"	\
			"ipaddr=202.197.67.2\0"	\
			"gatewayip=202.197.67.1\0"	\
			"netmask=255.255.255.0\0"	\
			"serverip=202.197.67.3\0"	\
			"stdin=serial,usbkbd\0" \
			"stdout=serial\0" \
			"stderr=serial\0"	\
			"ext4_read_test=ext4load scsi 0:1 0x90000000 uImage\0"	\
			"ext4_write_test=ext4write scsi 0:1 0x90000000 /uImage_back 0x13D5848\0"	\
			"ext4_read_check=ext4load scsi 0:1 0x92000000 uImage_back\0"	\
			"fat_read_test=fatload usb 0:1 0x90000000 md5sum.txt\0"  \
			"fat_write_test=fatwrite usb 0:1 0x90000000 /file_bake.txt 0x5D4A\0" \
			"fat_read_check=fatload usb 0:1 0x92000000 file_bake.txt\0"  \
			"load_kernel_kylin=ext4load scsi 0:1 0x90100000 uImage\0"	\
			"load_kernel_phy=ext4load scsi 0:1 0x90100000 ft/Image\0"	\
			"boot_fdt_kylin=bootm 0x90100000 -:- 0x90000000\0"	\
			"boot_os=booti 0x90100000 -:- 0x90000000\0"	\
			"load_kernel_vxworks=ext4load scsi 0:1 0x80100000 vxWorks.bin\0"	\
			"boot_vxworks=bootvx32 0x80100000\0"	\
			"vxworks_bootcmd=run load_kernel_vxworks; run boot_vxworks\0"	\
			"kylin_bootcmd=setenv bootargs 'console=ttyAMA0,115200 earlycon=pl011,0x28001000 root=/dev/sda2 rw'; run load_kernel_kylin; run load_fdt_kylin; run boot_fdt_kylin\0"    \
			"phytium_bootcmd=setenv bootargs 'console=ttyAMA1,115200 earlycon=pl011,0x28001000 root=/dev/sda5 rw';run load_kernel_phy; run load_fdt_phy; run boot_os\0"         \
			"load_kernel=ext4load scsi 0:1 0x90100000 uImage-2004\0"	\
			"boot_fdt=bootm 0x90100000 -:- 0x17C000\0"	\
			"distro_bootcmd=run load_kernel; run boot_fdt"

//#define CONFIG_BOOTARGS		"console=ttyAMA0,115200 earlycon=pl011,0x28001000 root=/dev/sda2 rw"

#ifdef  CONFIG_TRUSTED_BOOT
#define TRUSTED_BOARD_BOOT 1
#define USE_TBBR_DEFS 1
#endif

#endif
