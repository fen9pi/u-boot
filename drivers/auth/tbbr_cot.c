/*
 * Copyright (c) 2015-2017, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <auth_mod.h>
#include <platform_def.h>
#include <stddef.h>


#include <tbbr_oid.h>
/*
 * Maximum key and hash sizes (in DER format)
 */
#define PK_DER_LEN			91
#define HASH_DER_LEN			50

/*
 * The platform must allocate buffers to store the authentication parameters
 * extracted from the certificates. In this case, because of the way the CoT is
 * established, we can reuse some of the buffers on different stages
 */
static auth_param_type_desc_t nt_fw_content_pk = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_PUB_KEY, NON_TRUSTED_FW_CONTENT_CERT_PK_OID);
static auth_param_type_desc_t nt_world_bl_hash = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_HASH, NON_TRUSTED_WORLD_BOOTLOADER_HASH_OID);

static unsigned char nt_world_bl_hash_buf[HASH_DER_LEN];


/*
 * Parameter type descriptors
 */

static auth_param_type_desc_t non_trusted_nv_ctr = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_NV_CTR, NON_TRUSTED_FW_NVCOUNTER_OID);

static auth_param_type_desc_t sig = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_SIG, 0);
static auth_param_type_desc_t sig_alg = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_SIG_ALG, 0);
static auth_param_type_desc_t raw_data = AUTH_PARAM_TYPE_DESC(
		AUTH_PARAM_RAW_DATA, 0);

/*
 * TBBR Chain of trust definition
 */
static const auth_img_desc_t cot_desc[] = {
	/*
	 *Public Key BL3x
	 */
	[BL3X_PUBKEY_KEY_CERT_ID] = {
		.img_id = BL3X_PUBKEY_KEY_CERT_ID,
		.img_type = IMG_CERT,
		.parent = NULL,
		.img_auth_methods = {
			[0] = {
				.type = AUTH_METHOD_SIG,
				.param.sig = {
					.pk = &nt_fw_content_pk,
					.sig = &sig,
					.alg = &sig_alg,
					.data = &raw_data,
				}
			},
			[1] = {
				.type = AUTH_METHOD_NV_CTR,
				.param.nv_ctr = {
					.cert_nv_ctr = &non_trusted_nv_ctr,
					.plat_nv_ctr = &non_trusted_nv_ctr
				}
			}
		},
		.authenticated_data = {
			[0] = {
				.type_desc = &nt_world_bl_hash,
				.data = {
					.ptr = (void *)nt_world_bl_hash_buf,
					.len = (unsigned int)HASH_DER_LEN
				}
			}
		}
	},

	[BL3X_PUBKEY_IMG_ID] = {
		.img_id = BL3X_PUBKEY_IMG_ID,
		.img_type = IMG_RAW,
		.parent = &cot_desc[BL3X_PUBKEY_KEY_CERT_ID],
		.img_auth_methods = {
			[0] = {
				.type = AUTH_METHOD_HASH,
				.param.hash = {
					.data = &raw_data,
					.hash = &nt_world_bl_hash,
				}
			}
		}
	},

	/*KERNEL IMG*/
	[NT_KERNEL_KEY_CERT_ID] = {
		.img_id = NT_KERNEL_KEY_CERT_ID,
		.img_type = IMG_CERT,
		.parent = NULL,
		.img_auth_methods = {
			[0] = {
				.type = AUTH_METHOD_SIG,
				.param.sig = {
					.pk = &nt_fw_content_pk,
					.sig = &sig,
					.alg = &sig_alg,
					.data = &raw_data,
				}
			},
			[1] = {
				.type = AUTH_METHOD_NV_CTR,
				.param.nv_ctr = {
					.cert_nv_ctr = &non_trusted_nv_ctr,
					.plat_nv_ctr = &non_trusted_nv_ctr
				}
			}
		},
		.authenticated_data = {
			[0] = {
				.type_desc = &nt_world_bl_hash,
				.data = {
					.ptr = (void *)nt_world_bl_hash_buf,
					.len = (unsigned int)HASH_DER_LEN
				}
			}
		}
	},
	
	[NT_KERNEL_IMG_ID] = {
		.img_id = NT_KERNEL_IMG_ID,
		.img_type = IMG_RAW,
		.parent = &cot_desc[NT_KERNEL_KEY_CERT_ID],
		.img_auth_methods = {
			[0] = {
				.type = AUTH_METHOD_HASH,
				.param.hash = {
					.data = &raw_data,
					.hash = &nt_world_bl_hash,
				}
			}
		}
	},

};

/* Register the CoT in the authentication module */
REGISTER_COT(cot_desc);

