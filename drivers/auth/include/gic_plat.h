#ifndef _GIC_PLAT_H_
#define _GIC_PLAT_H_

/*
 * =====================================================================================
 *
 *       Filename:  gic_plat.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2019年02月20日 16时29分11秒
 *       Revision:  none
 *
 *         Author:  lixinde (lxd), lixinde@phytium.com.cn
 *        Company:  Phytium Technology Co.,Ltd
 *        License:  GPL-2.0+
 *
 * =====================================================================================
 */

#include <platform_def.h>
#include <interrupt_props.h>
#include <gic_common.h>

/*****************************************************************************************/
/****************************************GIC**********************************************/
/*****************************************************************************************/


#define PLAT_phytium_G1S_IRQ_PROPS(grp)										\
	INTR_PROP_DESC(TSP_IRQ_SEC_PHY_TIMER, GIC_HIGHEST_SEC_PRIORITY, (grp), GIC_INTR_CFG_LEVEL),	\
	INTR_PROP_DESC(PK_SECURE_ID, GIC_HIGHEST_SEC_PRIORITY, (grp), GIC_INTR_CFG_LEVEL),

#define PLAT_phytium_G0_IRQ_PROPS(grp)										\
	INTR_PROP_DESC(WAKEUP_INT_ID, GIC_HIGHEST_SEC_PRIORITY, (grp), GIC_INTR_CFG_LEVEL),

#endif
