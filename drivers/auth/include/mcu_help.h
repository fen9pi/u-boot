#ifndef	_MCU_HELP_H_
#define _MCU_HELP_H_
#include <stdio.h>
#include <stdint.h>
#include <phytium_sip.h>
/*
 * =====================================================================================
 *
 *       Filename:  mcu_help.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2019年02月20日 16时29分11秒
 *       Revision:  none
 *
 *         Author:  lixinde (lxd), lixinde@phytium.com.cn
 *        Company:  Phytium Technology Co.,Ltd
 *        License:  GPL-2.0+
 *
 * =====================================================================================
 */


void self_refresh(void);

#endif
