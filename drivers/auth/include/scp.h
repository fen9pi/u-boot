/*
 * Copyright (c) 2014-2017, ARM Limited and Contributors. All rights reserved.
 *
 * 	SCP提供外部调用接口
 */

#ifndef __SCPI_CMD_H__
#define __SCPI_CMD_H__
#include <bl_common.h>

u_register_t psci_phytium_scpi(u_register_t x1, u_register_t x2, u_register_t x3);

int scpi_wfi_enable(void);

int bl1_plat_handle_scp_bl2(image_info_t *scp_bl2_image_info);

uint64_t scp_get_clock(uint32_t id);
int scp_local_reset(uint32_t source);
int scp_local_pwrctr(u_register_t id, u_register_t operate);

#endif	/* __SCPI_CMD_H__ */
