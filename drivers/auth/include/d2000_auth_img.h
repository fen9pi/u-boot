/*
 * Copyright (c) 2015-2018, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __D2000_AUTH_IMG_H__
#define __D2000_AUTH_IMG_H__

#include <auth_common.h>
#include <cot_def.h>
#include <img_parser_mod.h>
#include <tbbr_img_def.h>


/* Public functions */
//void auth_public_keys(uint32_t img_id);
void auth_public_keys(uint32_t img_id,unsigned long pubkey_bin_addr,unsigned long bin_len,unsigned long pubkey_crt_addr,unsigned long pubkey_crt_len,unsigned long img_crt_addr,unsigned long img_crt_len);
int auth_rollback_counter(void);
int auth_image(unsigned int image_id, 
			uintptr_t image_base,
			uintptr_t image_size);

#endif /* __D2000_AUTH_IMG_H__ */
